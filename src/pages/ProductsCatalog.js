import ProductCard from '../components/ProductCard';

import { useState, useEffect } from 'react';

import {Container, Row, Col} from 'react-bootstrap'

export default function ProductsCatalog() {
let count = 0;
const [products, setProducts] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/allProducts`)
	.then(res=>res.json()).then(data => {
		//console.log(data);
		setProducts(data.map(product => {
			count++
			return (
				<Col lg={4} className="mb-2" key={count}>
				<ProductCard key={product.id} product={product}/>
				</Col>
			) 
		}))
	})
	})
	

	return (
		<>
		<Container fluid>
		<Row>
		{products}
		</Row>
		</Container>
		</>
	)
}
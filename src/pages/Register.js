import { useState, useEffect, useContext } from 'react';

import {Navigate, useNavigate } from 'react-router-dom';

import UserContext from '../UserContext';

import { Form, Button, Col } from 'react-bootstrap';

import Swal from 'sweetalert2';

export default function Register() {

    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [name, setName] = useState('');
    const [isActive, setIsActive] = useState(false);
    const {user} = useContext(UserContext);
    const navigate = useNavigate();

    function registerUser(e) {

        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: email
            })
        }).then(res => res.json()).then(data => {
            console.log(data);
            if(data){
                Swal.fire({
                    title: "Registration Failed",
                    icon: "error",
                    text: "Email already exists. Please use another email"
                })
            } else {

                fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                    method: 'POST',
                    headers:{
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        name: name,
                        email: email,
                        password: password1
                    })
                }).then(res => res.json()).then(data => {
                    console.log(data);
                    if(data){
                        Swal.fire({
                            title: "Registration Successful",
                            icon: "success",
                            text: "Welcome to BuyAnything!"
                        })
                        navigate('/login')    
                    } else{
                        Swal.fire({
                            title: "Registration Failed",
                            icon: "error",
                            text: "Please check your details and try again."
                        })
                    }
                    

                })
                

            }
        })
        
        setEmail('');
        setPassword1('');
        setPassword2('');
        setName('');
        console.log(name)
     
    }
   
    useEffect(() => {
        if(email && password1 === password2 && password1 !== '' && name){
            setIsActive(true);
        }else{
            setIsActive(false);
        }
    }, [email, password1, password2, name])


    return (
        (user.id !== null)
        ?
        <Navigate to="/"/>
        :
        <Col lg={{span:6, offset:3}} className="cardBg text-center p-3 text-new">
        <Form onSubmit={(e) => registerUser(e)}>
            {/*Name*/}
        <h1 className="m-2" >Registration</h1>
        <Form.Group controlId="userName" className="mb-2">
        <Form.Label>Name</Form.Label>
        <Form.Control 
        type="text" 
        placeholder="Enter name"
        value={name}
        onChange={e => setName(e.target.value)} 
        required
        />
        </Form.Group>

        <Form.Group controlId="userEmail" className="mb-2">
        <Form.Label>Email address</Form.Label>
        <Form.Control 
        type="email" 
        placeholder="Enter email"
        value={email}
        onChange={e => setEmail(e.target.value)} 
        required
        />
        <Form.Text className="text-muted">
        We'll never share your email with anyone else.
        </Form.Text>
        </Form.Group>


        <Form.Group controlId="password1" className="mb-2">
        <Form.Label>Password</Form.Label>
        <Form.Control 
        type="password" 
        placeholder="Password"
        value={password1}
        onChange={e => setPassword1(e.target.value)}
        required
        />
        </Form.Group>

        <Form.Group controlId="password2" className="mb-3">
        <Form.Label>Verify Password</Form.Label>
        <Form.Control 
        type="password" 
        placeholder="Verify Password"
        value={password2}
        onChange={e => setPassword2(e.target.value)}
        required
        />
        </Form.Group>

        { isActive ?
        <Button type="submit" id="submitBtn">
        Submit
        </Button>
        :
        <Button type="submit" id="submitBtn" disabled>
        Submit
        </Button>
    }
    </Form>
    </Col>
    )

}
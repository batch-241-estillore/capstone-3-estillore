import ProductPreview from '../components/ProductPreview';
import ProductsCatalog from './ProductsCatalog';
import React from 'react'
export default function Home(){
	return (
		<>
			
			<ProductPreview/><br></br>
			<h2 className="text-center textHome">BuyAnything </h2>
			<h3 className="text-center">starts here:</h3>
			<br></br>

			<ProductsCatalog/>
		</>
	)
}
export default function Error() {
  

	return (
		<>
		<h1>Error 404: Page not found</h1>
		<p>Back to <a href="/">home</a>.</p>
		</>
	)
}
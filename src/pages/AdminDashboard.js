import ProductTable from '../components/ProductTable';
import UserTable from '../components/UserTable';


import Table from 'react-bootstrap/Table';
import {Button, Col} from 'react-bootstrap';
import {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';

export default function AdminDashboard(){

	const [products, setProducts] = useState([]);
	const [users, setUsers] = useState([]);
	const [isTable, setIsTable] = useState(true);

	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/products/allProductsAdmin`)
		.then(res=>res.json()).then(data => {

			setProducts(data.map(product => {
				//console.log(product._id)
				return (
					<ProductTable key={product._id} product={product}/>
					)
			}))
		})

		fetch(`${process.env.REACT_APP_API_URL}/users/allUsers`)
		.then(res=>res.json()).then(data => {
			setUsers(data.map(client => {
				return (
					<UserTable key={client._id} client={client}/>
					)
			}))
		})
	}, [])

	let changeState = (e)=> {
		e.preventDefault();
		(isTable)?setIsTable(false):setIsTable(true)
	}

	return(
		<>
		<Col lg={{span:8, offset:2}} className="cardBg text-center p-3 text-new">
		<h1>Admin Dashboard</h1>
		<Button as={Link} to="/addProduct" className="m-3" >Add Product</Button>
		{(isTable)?
		<>
		<Button className="m-3" onClick={changeState}>View All Users</Button>
		<Table striped bordered hover variant="dark" className="mt-3">
		<thead>
		<tr>
		<th>id</th>
		<th>Product Name</th>
		<th>Description</th>
		<th>Price</th>
		<th>isActive</th>
		<th>Actions</th>
		</tr>
		</thead>
		<tbody>
		{products}		
		</tbody>
		</Table>
		</>
		:
		<>
		<Button className="m-3" onClick={changeState}>View All Products</Button>
		<Table striped bordered hover variant="dark" className="mt-3">
		<thead>
		<tr>
		<th>id</th>
		<th>User Name</th>
		<th>Email</th>
		<th>isAdmin</th>
		<th>Action</th>
		</tr>
		</thead>
		<tbody>
		{users}		
		</tbody>
		</Table>
		</>
		}
		

		
		</Col>
		</>
		);
}


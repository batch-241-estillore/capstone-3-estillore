import { useState, useEffect, useContext } from 'react';

import { Form, Button, Col } from 'react-bootstrap';

import UserContext from '../UserContext';

import { Navigate } from 'react-router-dom';

import Swal from 'sweetalert2';

export default function Login() {
    //Allows us to use the UserContext object and its properties to be used for user validation
    const {user, setUser} = useContext(UserContext);


    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);
    


    //hook returns a function that lets you navigate to components
    //const navigate = useNavigate();

    function authenticate(e) {
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })

        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            //console.log(data.name);

            if(typeof data.accessToken !== "undefined"){
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken);

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to BuyAnything!"
                })
                                
            } else {
                Swal.fire({
                    title: "Authentication Failed",
                    icon: "error",
                    text: "Please, check your login details and try again."
                })
            }
        })
        
       

        setEmail('');
        setPassword('');
    
    
    }

    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data.name);

            setUser({
                id: data._id,
                isAdmin: data.isAdmin,
                name: data.name
            })
         
        })
    }

    useEffect(() => {
        if(email && password){
            setIsActive(true);
        }else{
            setIsActive(false);
        }
    }, [email, password])

    return (
        (user.id !== null)?
        (user.isAdmin)?
        <Navigate to="/adminDashboard"/>
        :
        <Navigate to="/"/>
        :
        <Col lg={{span:6, offset:3}} className="cardBg text-center p-3 text-new">
        <Form onSubmit={(e) => authenticate(e)}>
        <Form.Group controlId="userEmail" className="mb-2">
        <h1>Login</h1>
        <Form.Label>Email address</Form.Label>
        <Form.Control 
        type="email" 
        placeholder="Enter email"
        value={email}
        onChange={e => setEmail(e.target.value)} 
        required
        />

        </Form.Group>

        <Form.Group controlId="password" className="mb-3">
        <Form.Label>Password</Form.Label>
        <Form.Control 
        type="password" 
        placeholder="Password"
        value={password}
        onChange={e => setPassword(e.target.value)}
        required
        />
        </Form.Group>


        { isActive ?
        <Button type="submit" id="submitBtn">
        Login
        </Button>
        :
        <Button type="submit" id="submitBtn" disabled>
        Login
        </Button>
    }
    </Form>
    </Col>
    )

}
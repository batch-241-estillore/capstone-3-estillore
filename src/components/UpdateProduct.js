import ProductCard from './ProductCard';
import UserContext from '../UserContext';


import {useEffect, useState, useContext} from 'react';

import {Form, Button} from 'react-bootstrap';

import {useParams, Navigate, useNavigate} from 'react-router-dom';

import Swal from 'sweetalert2';


export default function UpdateProduct(){

	const {productId} = useParams();
	const {user} = useContext(UserContext);
	const navigate = useNavigate();

	const [product, setProduct] = useState({});
	const {name, description, price, img} = product;

    const [isUpdate, setIsUpdate] = useState(false);


	const [productName, setProductName] = useState(name);
	const [prodDescription, setProdDescription] = useState(description);
	const [productPrice, setProductPrice] = useState(price);
	const [productImg, setProductImg] = useState(price);

	function updateProduct(e){
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products/update/${productId}`, {
			method: 'PATCH',
			headers: {
				Authorization: `Bearer ${localStorage.getItem(`token`)}`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				name: productName,
				description: prodDescription,
				price: productPrice,
				img: productImg
			})
		}).then(res=>res.json()).then(data => {
			console.log(data);
			if(data){
				Swal.fire({
                    title: "Update Successful",
                    icon: "success",
                    text: "Product was successfully updated"
                })
				navigate('/adminDashboard');
			}else{
				Swal.fire({
                    title: "Update failed",
                    icon: "success",
                    text: "There was a problem updating product. Please check details and try again."
                })
			}
		})

		setProductName(name);
		setProdDescription(description);
		setProductPrice(price);
		setProductImg(img);

	}


	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res=>res.json()).then(data => {
			// console.log(data);
			setProduct(data);
			
		})
	},[productId, name, description, price]);
	// console.log(product)

	useEffect(() => {
        if(productName || prodDescription || productPrice){
            setIsUpdate(true);
        }else{
            setIsUpdate(false);
        }
    }, [productName, prodDescription, productPrice])


	return (
		(user.isAdmin)?
		<>
		<h2>Product Info:</h2>
		<ProductCard product={product}/>

		<Form onSubmit={(e) => updateProduct(e)}>
        <Form.Group controlId="productName">
        <h3>Update this product:</h3>
        <Form.Label>Product Name</Form.Label>
        <Form.Control 
        type="text" 
        placeholder={name}
        value={productName}
        onChange={e => setProductName(e.target.value)} 
        
        />

        </Form.Group>

        <Form.Group controlId="description">
        <Form.Label>Description:</Form.Label>
        <Form.Control 
        type="text" 
        placeholder={description}
        value={prodDescription}
        onChange={e => setProdDescription(e.target.value)}
        
        />
        </Form.Group>

         <Form.Group controlId="price">
        <Form.Label>Price:</Form.Label>
        <Form.Control 
        type="number" 
        placeholder={price}
        value={productPrice}
        onChange={e => setProductPrice(e.target.value)}
        
        />
        </Form.Group>

        <Form.Group controlId="img">
        <Form.Label>Image URL:</Form.Label>
        <Form.Control 
        type="text" 
        placeholder={img}
        value={productImg}
        onChange={e => setProductPrice(e.target.value)}
        
        />
        </Form.Group>

        { isUpdate ?
        <Button variant="primary" type="submit" id="submitBtn">
        Update
        </Button>
        :
        <Button variant="primary" type="submit" id="submitBtn" disabled>
        Update
        </Button>
		}

        </Form>
		</>
		:
		<Navigate to="/"/>
	)
}
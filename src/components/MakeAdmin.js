import Swal from 'sweetalert2';
import {Navigate, useLocation}from 'react-router-dom';

export default function MakeAdmin(){
	const location = useLocation();
	let {id} = location.state;
   
    fetch(`${process.env.REACT_APP_API_URL}/users/adminUpgrade`, {
      method: 'PATCH',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        userId: id
      })
    }).then(res=>res.json()).then(data=>{
      console.log(data)
      console.log(id)
      if(data){
        Swal.fire({
                    title: "Successfully Promoted User!",
                    icon: "success",
                    text: "This user is now an admin just like you!"
                })
      }else{
        Swal.fire({
                    title: "Unable to make this user admin",
                    icon: "error",
                    text: "There was an error in promoting this user."
                })
      }
    })

    return(
    	<Navigate to="/adminDashboard"/>
    )
}
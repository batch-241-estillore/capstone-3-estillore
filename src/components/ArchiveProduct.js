import Swal from 'sweetalert2';
import {Navigate, useParams, useLocation}from 'react-router-dom';

export default function ArchiveProduct(){

		const {productId} = useParams();
		const location = useLocation();
		let {isActive} = location.state
		isActive = (!isActive);

		fetch(`${process.env.REACT_APP_API_URL}/products/archive/${productId}`, {
			method: 'PATCH',
			headers: {
				Authorization: `Bearer ${localStorage.getItem(`token`)}`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				isActive: isActive
			})
		}).then(res => res.json()).then(data => {
			console.log(data)
			if(data){
				Swal.fire({
                    title: "Successfully Changed Active Status!",
                    icon: "success",
                    text: "This product's status has been changed."
                })
			}else{
				Swal.fire({
                    title: "Failed to change status of product",
                    icon: "error",
                    text: "Something went wrong. Please try again"
                })
			}
		})
	return(
			<Navigate to='/adminDashboard'/>
		)
}
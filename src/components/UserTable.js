import {Button} from 'react-bootstrap';


import {Link} from 'react-router-dom';


export default function UserTable({client}) {
  const {_id, name, email, isAdmin} = client;
  console.log(_id)

  return (

      
        <tr>
          <td>{_id}</td>
          <td>{name}</td>
          <td>{email}</td>        
          {(isAdmin)?
          <td className="text-success">Admin</td>
          :
          <td className="text-danger">Not Admin</td>
          }
          
          <td>
            {(!isAdmin)?
            <Button as={Link} to="/makeAdmin" state={{id:_id}} variant="primary">Promote</Button>
            :
            <Button variant="secondary" disabled>Demote</Button>
            }
          </td>
        </tr>      
      
  );
}


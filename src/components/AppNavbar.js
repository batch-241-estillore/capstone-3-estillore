
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

import {Link, NavLink} from 'react-router-dom';
import {useContext} from 'react'

import UserContext from '../UserContext'



export default function AppNavbar() {

  const {user} = useContext(UserContext);
  console.log(user);

  return (

    <Navbar id="navbarMain" sticky="top" expand="lg">
    	
        <Navbar.Brand id="navLogo" as={Link} to="/">BuyAnything</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ml-auto">
            
            {(user.id !== null)?
            <>
              {(user.isAdmin)?
              <Nav.Link id="text-new" as={NavLink} to="/adminDashboard">Admin Dashboard</Nav.Link>
              :
              <Nav.Link id="text-new" as={NavLink} to="/productsCatalog">Products</Nav.Link>          
              }
              <Nav.Link id="text-new" as={NavLink} to="/logout">Logout</Nav.Link>
            </>
              :
              <>
              <Nav.Link id="text-new" as={NavLink} to="/register">Register</Nav.Link>
              <Nav.Link id="text-new" as={NavLink} to="/login">Login</Nav.Link>
              </>
            }
            
          </Nav>
          {(user.id == null) ?
          <div> </div>
          :
          <Navbar.Text id="signedName" className="ms-auto">
            Signed in as: {user.name}
          </Navbar.Text>
          }
          
        </Navbar.Collapse>
        
    </Navbar>
  )

}
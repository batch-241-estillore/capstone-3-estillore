import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, InputGroup, Form } from 'react-bootstrap';
import { useParams, useNavigate } from 'react-router-dom';

import UserContext from '../UserContext';


import Swal from 'sweetalert2'

export default function ProductView() {

	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	
	const {productId} = useParams();
	
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [img, setImg] = useState('');
	const [quantity, setQuantity] = useState(0)

	function checkout(productId){
		fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				userId: user.id,
				quantity: quantity
			})
		})
		.then(res => res.json()).then(data => {
			console.log(data);
			console.log(quantity)
			if(data === true){
				Swal.fire({
					title: "Successfully purchased",
					icon: "success",
					text: "You have successfully purchased this product."
				})
				navigate("/productsCatalog");
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}


		})
		setQuantity(0);
	}




	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res=>res.json()).then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setImg(data.img);

		})
	}, [productId])

	return (

		<Container fluid className="text-center">
		
			<img alt="insertImgHere" className="cardBg" id="checkoutImg" src={img}/>
			
		<Row className="text-center">
		<Col lg={{span: 6, offset:3}} >
		<Card>
		<Card.Body>
		<Card.Title>{name}</Card.Title>
		<Card.Subtitle>Description:</Card.Subtitle>
		<Card.Text>{description}</Card.Text>
		<Card.Subtitle>Price:</Card.Subtitle>
		<Card.Text>PhP {price}</Card.Text>
		<Col xs={{span: 4, offset:4}}>
		<InputGroup>
		<Button variant="outline-secondary" type="button" onClick={()=> {if(quantity>0)setQuantity(quantity-1)}}>-</Button>
        <Form.Control       
          value={quantity} onChange={(e) => setQuantity(e.target.value)}
        />
        
        <Button variant="outline-secondary" type="button" onClick={()=>setQuantity(quantity+1)}>+</Button>
      </InputGroup>
 		</Col>
 		<br></br>

		{
			(user.id !== null && quantity>0) ?
			<Button variant="primary" onClick={() => checkout(productId)} >Checkout</Button>
			:
			<Button variant="primary" disabled>Checkout</Button>
		}

		</Card.Body>
		</Card>
		</Col>
		</Row>
		</Container>

		)
}
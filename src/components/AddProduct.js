import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import {Col} from 'react-bootstrap';
import InputGroup from 'react-bootstrap/InputGroup'

import { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';

import UserContext from '../UserContext'

import Swal from 'sweetalert2';

export default function AddProduct(){
	const {user} = useContext(UserContext)
	const [productName, setProductName] = useState('');
	const [description, setDescription] = useState('');
	const [productPrice, setProductPrice] = useState(null);
	const [productImg, setProductImg] = useState('');


	const [isActive, setIsActive] = useState(false);
	const [productAdded, setProductAdded] = useState(false);
	const [token, setToken] = useState('');


	const navigate = useNavigate();

	function productRegister(e){
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products/checkProductName`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				name: productName
			})
		}).then(res=>res.json()).then(data => {
			console.log(data)
			if(data){
				Swal.fire({
                    title: "Failed to add product",
                    icon: "error",
                    text: "Product name already exists. Please use another name"
                })
			}else{
				fetch(`${process.env.REACT_APP_API_URL}/products/addProduct`, {
					method: 'POST',
					headers: {						
						'Content-Type': 'application/json',
						Authorization: `Bearer ${token}`
					},
					body: JSON.stringify({
						name: productName,
						description: description,
						price: productPrice,
						img: productImg
					})
				}).then(res=>res.json()).then(data => {
					if(data){
						console.log(data)
						Swal.fire({
                            title: "Successfully added new product!",
                            icon: "success",
                            text: `Added product: ${productName}!`
                        })
                        setProductAdded(true);
					}else{
						Swal.fire({
                            title: "Failed to add product",
                            icon: "error",
                            text: "Please check your details and try again."
                        })
					}
				})
			}
		})

		setProductName('');
		setDescription('');
		setProductImg('');
		setProductPrice(null);

	}

	useEffect(() => {
		if(productName && description && productPrice !== 0){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	}, [productName, description, productPrice]);

	useEffect(() => {
		if(productAdded){
			navigate("/adminDashboard")
		}
	},[productAdded, navigate]);

	useEffect(() => {
		const token = localStorage.getItem('token');
		if (token){
			setToken(token);
		}
	},[])
	return(
		(user.isAdmin)?
		<>
		<Col lg={{span:6, offset:3}} className="cardBg text-center p-3 text-new">

		<h1 className="text-center">Add Product</h1>

		<Form onSubmit={(e) => productRegister(e)}>
			<Form.Group className="mb-3" controlId="formBasicEmail">
				<Form.Label>Product Name:</Form.Label>
				<Form.Control type="text" value={productName} onChange={e=> setProductName(e.target.value)} placeholder="Enter product name" />
			</Form.Group>

			<Form.Group className="mb-3" controlId="formDescription">
				<Form.Label>Description</Form.Label>
				<Form.Control type="text" value={description} onChange={e=> setDescription(e.target.value)} placeholder="Short product description" />
			</Form.Group>

			<Form.Group className="mb-3" controlId="formPrice">
				<Form.Label>Price</Form.Label>
				<InputGroup>
				<InputGroup.Text>₱</InputGroup.Text>
				<Form.Control type="number" value={productPrice} onChange={e=> setProductPrice(e.target.value)} placeholder="Input price amount" />
				</InputGroup>
			</Form.Group>

			<Form.Group className="mb-3" controlId="formImage">
				<Form.Label>Image URL</Form.Label>
				<Form.Control type="text" value={productImg} onChange={e=> setProductImg(e.target.value)} placeholder="Place image url here" />
			</Form.Group>

			{(isActive)?
			<Button id="submitBtn" type="submit">
			Submit
			</Button>
			:
			<Button id="submitBtn" type="submit" disabled>
			Submit
			</Button>
			}
			
		</Form>
		</Col>
		</>
		:
		<Navigate to="/productsCatalog"/>

		
	)
}
import {Button} from 'react-bootstrap';

import {Link} from 'react-router-dom';


export default function ProductTable({product}) {
  const {_id, name, description, price, isActive} = product;
  console.log(product);
  return (

      
        <tr>
          <td>{_id}</td>
          <td>{name}</td>
          <td>{description}</td>
          <td>{price}</td>
          {(isActive)?
          <td className="text-success">Active</td>
          :
          <td className="text-danger">Inactive</td>
          }
          
          <td>
            <Button as={Link} to={`/updateProduct/${_id}`}>Update</Button>
            {(isActive)?
            <Button as={Link} to={`/archiveProduct/${_id}`} state={{isActive: true}} variant="danger">Archive</Button>
            :
            <Button as={Link} to={`/archiveProduct/${_id}`} state={{isActive: false}} variant="success">Unarchive</Button>
            }
          </td>
        </tr>      
      
  );
}


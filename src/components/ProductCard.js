import {Card, Button} from 'react-bootstrap'
import {Link} from 'react-router-dom'

import {useContext} from 'react'
import UserContext from '../UserContext';

export default function ProductCard({product}) {
	const {user} = useContext(UserContext);
	const {name, description, _id, img} = product;
	//console.log(product);
	//console.log(_id);


	return (
	<div id="cardBg">
	<Card className="bg-dark text-white">
	<Card.Img className="cardImg" src={img}/>
	
		<Card.ImgOverlay>
			<Card.Title>{name}</Card.Title>
			<Card.Text>{description}</Card.Text>
		</Card.ImgOverlay>
		{(user.id==null)?
		<></>
		:
		(user.isAdmin)?
		<></>
		:
		<Button id="buttonProductView" as={Link} to={`/productView/${_id}`}>Details</Button>
		
		}
		
	</Card>
	</div>
	)
}
import React, { useState } from 'react';
import Carousel from 'react-bootstrap/Carousel';
import {Container} from 'react-bootstrap';
import Row from 'react-bootstrap/Row';


export default function ProductPreview() {

  const [index, setIndex] = useState(0);
 

  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex);
  };

  return (
    <Container fluid>
    <Row className="justify-content-center">
    <Carousel id="cardBg" className="w-50" variant="dark" activeIndex={index} onSelect={handleSelect}>
      <Carousel.Item>
        <img
          className="d-block carou1"
          src="https://images.pexels.com/photos/5872364/pexels-photo-5872364.jpeg?auto=compress&cs=tinysrgb&w=300"
          alt="First slide"
        />
        <Carousel.Caption className="text-white">
          <h3>50% Sale!</h3>
          <p>Get 50% off on all items when you register now!</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block carou1"
          src="https://images.pexels.com/photos/3293148/pexels-photo-3293148.jpeg?auto=compress&cs=tinysrgb&w=400"
          alt="Second slide"
        />

        <Carousel.Caption className="text-white">
          <h3>Summer Break!</h3>
          <p>Be ready to break the summer! Get your beach essentials here.</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block carou1"
          src="https://images.pexels.com/photos/11650184/pexels-photo-11650184.jpeg?auto=compress&cs=tinysrgb&w=300"
          alt="Third slide"
        />

        <Carousel.Caption className="text-white">
          <h3>BuyAnything Now!</h3>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
    </Row>
    </Container>
  );
}


import './App.css';

import {useState, useEffect} from 'react';

import {UserProvider} from './UserContext';


import AppNavbar from './components/AppNavbar.js';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import ProductsCatalog from './pages/ProductsCatalog';
import ProductView from './components/ProductView';
import AddProduct from './components/AddProduct';
import UpdateProduct from './components/UpdateProduct';
import ArchiveProduct from './components/ArchiveProduct';
import AdminDashboard from './pages/AdminDashboard';
import MakeAdmin from './components/MakeAdmin';
import Error from './pages/Error';


import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import {Container} from 'react-bootstrap';


function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
    name: null
  });


  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    }).then(res=> res.json())
    .then(data => {
      console.log(data);
      if(typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
          name: data.name
        })
      }else {
        setUser({
          id: null,
          isAdmin: null,
          name: null
        })
      }
    })
  }, [])

  return (
    <>
    <UserProvider value={ {user, setUser, unsetUser} }>
    <Router id="router">

      <AppNavbar/>

      <Container>
        <Routes>
          <Route path="/" element={<Home/>}/>
          <Route path="/register" element={<Register/>}/>
          <Route path="/login" element={<Login/>}/>
          <Route path="/logout" element={<Logout/>}/>
          <Route path="/productsCatalog" element={<ProductsCatalog/>}/>
          <Route path="/productView/:productId" element={<ProductView/>}/>
          <Route path="/addProduct" element={<AddProduct/>}/>
          <Route path="/updateProduct/:productId" element={<UpdateProduct/>}/>
          <Route path="/archiveProduct/:productId" element={<ArchiveProduct/>}/>
          <Route path="/adminDashboard" element={<AdminDashboard/>}/>
          <Route path="/makeAdmin" element={<MakeAdmin/>}/>
          <Route path="/*" element={<Error/>}/>
        </Routes>
      </Container>

    </Router>
    </UserProvider>
      
    </>
  );
}

export default App;
